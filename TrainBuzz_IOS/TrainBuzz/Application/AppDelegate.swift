//
//  AppDelegate.swift
//  TrainBuzz
//
//  Created by Salitha on 5/8/18.
//  Copyright © 2018 Salitha. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyDnegg23K9YASAc0uZNvwobX-YBEzOoOmA")
        GMSPlacesClient.provideAPIKey("AIzaSyDnegg23K9YASAc0uZNvwobX-YBEzOoOmA")
        
        FirebaseApp.configure()
        
         self.checkLogin()
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func checkLogin()
    {
        // Get the defaults
        let defaults = UserDefaults.standard
        let username  = defaults.object(forKey: "username_preference")
        let password = defaults.object(forKey: "password_preference")
        let automaticLogin = defaults.bool(forKey: "automatic_preference")
        
        if (automaticLogin == false || ((username == nil) && ((username as! String) != "")) || ((password == nil) && ((password as! String) != "")))
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            self.window?.rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.window?.makeKeyAndVisible()
        }
        else{
        
            let _ = TrainBuzzWebApi.AuthorizeUser(username! as! String, password: password! as! String) { (json,isloggedIn, error) -> Void in
                if (isloggedIn == true)
                {
                    DispatchQueue.main.async {
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        self.window?.rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainNavVc") as! MainNavVc
                        self.window?.makeKeyAndVisible()
                    }
                }}
        }
    }
    
    func NotificationAlert(title:String,msg:String,title2:String) {
        let topWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            self.window?.rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.window?.makeKeyAndVisible()
            
            topWindow.isHidden = true
        }))
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alert, animated: true)
    }

}

