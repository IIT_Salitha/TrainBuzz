    //
    //  Authorization.swift
    //  Peaberry
    //
    //  Created by PrasaD on 14/07/2015.
    //  Copyright (c) 2015 Bourke. All rights reserved.
    //
    
    import Foundation
    import SystemConfiguration
    
    class TrainBuzzWebApi {
        
        //local:http://192.168.0.21:3404            host:
        static let hostUrl : String = "http://35.200.178.94:8000/"
        
        static func AuthorizeUser(_ userName :String, password :String, completionHandler: @escaping (Auth?,Bool?, NSError?) -> Void ) -> URLSessionTask
        {
            var status : NSNumber = 0;
            var isLogginSuccess : Bool = false;
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)token")!)
            let session = URLSession.shared
            
            request.httpMethod = "POST"
            request.httpBody = "username=\(userName)&password=\(password)&grant_type=password".data(using: String.Encoding.utf8)
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil,isLogginSuccess,NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Username or password incorrect"]))
                    return
                }
                
                guard let data = data else {
                    completionHandler(nil,isLogginSuccess, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Username or password incorrect"]))
                    return }
                
                if let httpResponse = response as? HTTPURLResponse {
                     status = NSNumber(value:httpResponse.statusCode)
                }
                
                if status != 200{
                    completionHandler(nil,isLogginSuccess, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Username or password incorrect"]))
                    return
                }
                
                do {
                    
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let result = try decoder.decode(Auth.self, from: data)
                    
                    if(!(result.token!.isEmpty)){
                        let defaults = UserDefaults.standard
                        defaults.set(result.token, forKey: "access_token")
                        defaults.set(true, forKey: "automatic_preference")
                        defaults.synchronize()
                        isLogginSuccess = true;
                        completionHandler(result,isLogginSuccess, nil)
                    }
                    
                } catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    completionHandler(nil,isLogginSuccess, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Username or password incorrect"]))
                }
                return
            })
            
            task.resume()
            
            return task
        }
        
        static func GetRequest(_ url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            var status : NSNumber = 0;
            let session = URLSession.shared
            
            request.httpMethod = "GET"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil, error! as NSError)
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    status = NSNumber(value:httpResponse.statusCode)
                }
                
                if(status == 200 ){
                    completionHandler(data, nil)
                }
                else{
                    let err = NSError(domain: String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)), code: 0, userInfo: nil)
                    print(err)
                    completionHandler(nil, err)
                }
                
                return
            })
            
            task.resume()
            
            return task
        }
        
        static func GetStatusRequest(_ url:String, completionHandler: @escaping (NSNumber?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            var task = URLSessionTask();
            
            request.httpMethod = "GET"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil, error! as NSError)
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    status = NSNumber(value:httpResponse.statusCode)
                }
                
                if(status == 200 ){
                    completionHandler(status, nil)
                }
                else{
                    let err = NSError(domain: String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)), code: 0, userInfo: nil)
                    print(err)
                    completionHandler(nil, err)
                }
                return
            })
            
            task.resume()
            
            return task
        }
        
        static func GetAnnonymousRequest(_ url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
        
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil, error! as NSError)
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    status = NSNumber(value:httpResponse.statusCode)
                }
                
                if(status == 200 ){
                    completionHandler(data, nil)
                }
                else{
                    let err = NSError(domain: String(describing: NSString(data: data!, encoding: String.Encoding.utf8.rawValue)), code: 0, userInfo: nil)
                    print(err)
                    completionHandler(nil, err)
                }
                return
            })
            
            task.resume()
            
            return task
        }
        
        static func PostRequest(_ params : NSDictionary, url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            var task = URLSessionTask();
            
            request.httpMethod = "POST"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
                
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        status = NSNumber(value:httpResponse.statusCode)
                    }
                    
                    print(status)
                    if(status == 200 ){
                        completionHandler(data, nil)
                    }
                    else{
                        let err = NSError(domain: String(describing: NSString(data: data!, encoding:String.Encoding.utf8.rawValue)), code: 0, userInfo: nil)
                        print(err)
                        completionHandler(nil, error as NSError?)
                    }
                    
                    return
                })
                
                task.resume()
                
            } catch let error as NSError
            {
                print(error)
                completionHandler(nil, error)
            }
            
            return task
        }
        
        static func PostStatusRequest(_ params : NSDictionary, url:String, completionHandler: @escaping (NSNumber?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            var task = URLSessionTask();
            
            request.httpMethod = "POST"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        status = NSNumber(value:httpResponse.statusCode)
                    }
                    
                    completionHandler(status, error as NSError?)
                    return
                })
                
                task.resume()
            } catch let error as NSError
            {
                print(error)
            }
            
            return task
        }
        
        static func PostAnonymousRequest (_ params : NSDictionary, url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var task = URLSessionTask()
            
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
                
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200{
                            if(error != nil){
                                completionHandler(data, error as NSError?)
                            }else{
                                completionHandler(data, error as NSError?)
                            }
                            return
                        }else{
                            completionHandler(data, NSError(domain: "", code: httpResponse.statusCode, userInfo: nil))
                        }
                    }else{
                        completionHandler(data, NSError(domain: "", code: 400, userInfo: nil))
                    }
                })
                
                task.resume()
            } catch let error as NSError
            {
                print(error)
            }
            
            return task
        }
        
        static func PutRequest(_ params : NSDictionary, url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var task = URLSessionTask();
            
            request.httpMethod = "PUT"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.timeoutInterval = 250;
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200{
                            if(error != nil){
                                completionHandler(data, error as NSError?)
                            }else{
                                completionHandler(data, error as NSError?)
                            }
                            return
                        }else{
                            completionHandler(data, NSError(domain: "", code: httpResponse.statusCode, userInfo: nil))
                        }
                    }else{
                        completionHandler(data, NSError(domain: "", code: 400, userInfo: nil))
                    }
                })
                
                task.resume()
                
            } catch let error as NSError
            {
                print(error)
            }
            
            return task
        }
        
        static func PutStatusRequest(_ params : NSDictionary, url:String, completionHandler: @escaping (NSNumber?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            var task = URLSessionTask();
            
            request.httpMethod = "POST"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
                
               
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        status = NSNumber(value:httpResponse.statusCode)
                    }
                    
                    completionHandler(status, error as NSError?)
                    
                    return
                })
                
                task.resume()
                
            } catch let error as NSError
            {
                print(error)
            }
            
            return task
        }
        
        static func PutAnonymousRequest(_ params : NSDictionary, url:String, completionHandler: @escaping (Data?, NSError?) -> Void ) -> URLSessionTask
        {
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var task = URLSessionTask();
            
            request.httpMethod = "PUT"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
                
                task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    if let err = error {
                        print("Failed to get data from url:", err)
                        completionHandler(nil, error! as NSError)
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200{
                            if(error != nil){
                                completionHandler(data, error as NSError?)
                            }else{
                                completionHandler(data, error as NSError?)
                            }
                            return
                        }else{
                            completionHandler(data, NSError(domain: "", code: httpResponse.statusCode, userInfo: nil))
                        }
                    }else{
                        completionHandler(data, NSError(domain: "", code: 400, userInfo: nil))
                    }
                    
                })
                
                task.resume()
            } catch let error as NSError
            {
                print(error)
            }
            
            return task
        }
        
        static func DeleteStatusRequest(_ url:String, completionHandler: @escaping (NSNumber?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            
            request.httpMethod = "DELETE"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil, error! as NSError)
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    status = NSNumber(value:httpResponse.statusCode)
                }
                
                completionHandler(status, error as NSError?)
                
                return
            })
            
            task.resume()
            
            return task
            
        }
        
        static func DeleteRequest(_ url:String, completionHandler: @escaping (NSNumber?, NSError?) -> Void ) -> URLSessionTask
        {
            let token = UserDefaults.standard.string(forKey: "access_token")!
            let request = NSMutableURLRequest(url: URL(string: "\(hostUrl)\(url)")!)
            let session = URLSession.shared
            var status : NSNumber = 0;
            
            request.httpMethod = "DELETE"
            request.setValue("bearer \(token)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                if let err = error {
                    print("Failed to get data from url:", err)
                    completionHandler(nil, error! as NSError)
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    status = NSNumber(value:httpResponse.statusCode)
                }
                
                completionHandler(status, error as NSError?)
                
                return
            })
            
            task.resume()
            
            return task
        }
        
        static func isConnectedToNetwork() -> Bool {
            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
            
            guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
                
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                    
                    SCNetworkReachabilityCreateWithAddress(nil, $0)
                    
                }
                
            }) else {
                
                return false
            }
            var flags = SCNetworkReachabilityFlags()
            if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
                return false
            }
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            return (isReachable && !needsConnection)
        }
    }
    
    struct Auth: Decodable {
        let token: String?
    }
    
    
    
    
    
    
    
    
    
    
    
