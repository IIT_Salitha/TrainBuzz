//
//  DesignableTextField.swift
//  GiftCards
//
//  Created by lemon on 7/19/17.
//  Copyright © 2017 lemon. All rights reserved.
//

import UIKit

@IBDesignable

class DesignableTextField: UITextField {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var bordeWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = bordeWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var leftImage: UIImage?{
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var leftPaddingNoBorder: CGFloat = 0 {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var imageWidth: CGFloat = 0 {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var imageHeight: CGFloat = 0 {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: newValue!])
        }
    }
   
    func updateView()  {
        
        if let image = leftImage{
            leftViewMode = .always
            
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: imageWidth, height: imageHeight))
            imageView.image = image
            imageView.tintColor = tintColor
            var width = leftPadding + 20
            
            if borderStyle == UITextBorderStyle.none || borderStyle ==  UITextBorderStyle.line{
                width  = width + leftPaddingNoBorder
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: imageHeight))
            view.addSubview(imageView)
            leftView = view
        }
        else{
            leftViewMode = .never
        }
        
       // attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [NSForegroundColorAttributeName : tintColor])
    }
}
