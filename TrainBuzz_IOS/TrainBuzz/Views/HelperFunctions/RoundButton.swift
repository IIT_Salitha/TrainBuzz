//
//  RoundButton.swift
//  Jetsaver
//
//  Created by lemon on 9/12/17.
//  Copyright © 2017 lemon. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var bordeWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = bordeWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }

}
