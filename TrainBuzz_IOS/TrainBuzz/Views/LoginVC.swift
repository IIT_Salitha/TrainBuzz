//
//  ViewController.swift
//  TrainBuzz
//
//  Created by Salitha on 5/8/18.
//  Copyright © 2018 Salitha. All rights reserved.
//

import UIKit

class LoginVC: TrainBuzzController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viwContian: UIView!
    @IBOutlet weak var viwAnimation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        self.uiSetStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func loginBtnTap(_ sender: Any) {
        
//        if ((txtPassword.text?.isEmpty)! || (txtUsername.text?.isEmpty)!){
//            self.displayConfirmMessage("Error", message: "UserName and password is required.", actionTitle: "OK", isPopOver: false)
//            return
//        }
        
        let _ = TrainBuzzWebApi.AuthorizeUser("Roy", password: "123") { (json,isloggedIn, error) -> Void in
            if (isloggedIn == true)
            {
                DispatchQueue.main.async {
                    
                    let defaults = UserDefaults.standard
                    defaults.setValue("Roy", forKey: "username_preference")
                    defaults.setValue("123", forKey: "password_preference")
                    defaults.setValue(true, forKey: "automatic_preference")
                    defaults.synchronize()
                    
                    self.performSegue(withIdentifier: "main", sender: nil)
                    
                }
            }
            else {
                DispatchQueue.main.async {
                    self.displayConfirmMessage("Error", message: "UserName or Password was incorrect.Please try again.", actionTitle: "OK", isPopOver: false)
                    return
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showLogo()
    }
 
    func showLogo()  {
        UIView.animate(withDuration: 1, animations: {
            self.imgLogo.alpha = 1
            
        }, completion: { (true) in
            self.showHearder()
        })
    }
    
    func showHearder()  {
        UIView.animate(withDuration: 1, animations: {
            self.lblHeader.alpha = 1
        }, completion: { (true) in
            self.showTxtFeilds()
        })
    }
    
    func showTxtFeilds()  {
        UIView.animate(withDuration: 1, animations: {
            self.viwContian.alpha = 1
        })
    }
    
    func uiSetStyle()  {
        self.imgLogo.alpha = 0
        self.lblHeader.alpha = 0
        self.viwContian.alpha = 0
    }
}

