//
//  PeaberryController.swift
//  Peaberry
//
//  Created by PrasaD on 21/09/2015.
//  Copyright (c) 2015 Bourke. All rights reserved.
//

import UIKit
import CoreLocation

class TrainBuzzController: UIViewController {
    
    //MARK: Private variables
    var LoggedCompany: Int = 0
    var LoggedUser: Int = 0
    let MessageLength : Int = 256
    
    var mainThemColorDark = #colorLiteral(red: 0, green: 0.6832743883, blue: 0.4479640126, alpha: 1).cgColor
    var mainThemColorLight = #colorLiteral(red: 0.231372549, green: 0.7254901961, blue: 0.568627451, alpha: 1).cgColor
    
//    var backgroundColor: CGColor = UIColor(red: 114/255.0, green: 183/255.0, blue: 243/255.0, alpha: 1.0).cgColor
//
//    var appIconTopColor: CGColor = UIColor(red: 28/255.0, green: 231/255.0, blue: 234/255.0, alpha: 1.0).cgColor
//    var appIconBootmColor: CGColor = UIColor(red: 27/255.0, green: 224/255.0, blue: 242/255.0, alpha: 1.0).cgColor
//
//    var cellColor: CGColor = UIColor(red: 208/255.0, green: 230/255.0, blue: 251/255.0, alpha: 1.0).cgColor
//    var cellBrderColor: CGColor = UIColor(red: 185/255.0, green: 218/255.0, blue: 249/255.0, alpha: 1.0).cgColor
    
    var errorMessage: String = "Error occured. Please try again."
    
    //MARK: Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: Custom methods
    func displayAlertMessage(_ title: String, message: String){
        let myAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil );
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    func displayConfirmMessage(_ title: String, message: String, actionTitle: String, isPopOver: Bool){
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: actionTitle, style:.default, handler: { action in
            if(isPopOver)
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
               let _ = self.navigationController?.popViewController(animated: true)
            }
        })
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func SetTextFieldBorder(_ txtField: AnyObject, borderColor: CGColor){
        
        let loginborder = CALayer()
        let width = CGFloat(1.0)
        
        loginborder.borderColor = borderColor
        loginborder.borderWidth = width
        loginborder.frame = CGRect(x: 0, y: txtField.frame.size.height - width, width:  txtField.frame.size.width, height: txtField.frame.size.height)
        txtField.layer.addSublayer(loginborder)
        txtField.layer.masksToBounds = true        
    }
    
    func SetButtonBorder(_ btnField: AnyObject, borderColor: CGColor){
        btnField.layer.cornerRadius = 5
        btnField.layer.borderWidth = 1
        btnField.layer.borderColor = borderColor
    }
   
   
    
    func dismissKeyboardOnTap(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
//    func clearImageCache(){
//        let  imgCache : SDImageCache  = SDImageCache.shared()
//        imgCache.clearDisk();
//        imgCache.clearMemory();
//    }
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

open class LoadingOverlay{
    
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    open func showOverlay(_ view: UIView) {
        
        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.center = view.center
        overlayView.backgroundColor = UIColor(cgColor: TrainBuzzController().mainThemColorLight);
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        
        activityIndicator.startAnimating()
    }
    
    open func hideOverlayView() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
}


