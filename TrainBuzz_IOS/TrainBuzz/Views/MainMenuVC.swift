//
//  MainMenuVC.swift
//  TrainBuzz
//
//  Created by Salitha on 5/9/18.
//  Copyright © 2018 Salitha. All rights reserved.
//

import UIKit


class MainMenuVC: TrainBuzzController {

    @IBOutlet weak var txtArriving: UITextField!
    @IBOutlet weak var txtDeparting: UITextField!
    @IBOutlet weak var txtSchdule: UITextField!
    
    @IBOutlet weak var viwSchedule: UIView!
    @IBOutlet weak var btnTravel: RoundButton!
    @IBOutlet weak var btnSearchPlace: RoundButton!
    
    var selectTextField = UITextField()
    var pickeView = UIPickerView()
    var scheduleArr = Array<String>()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    // Mark - @IBAction
    @IBAction func searchBtnTap(_ sender: Any) {
        self.performSegue(withIdentifier: "map", sender: nil)
    }
    
    @IBAction func travelBtnTap(_ sender: Any) {
        self.performSegue(withIdentifier: "travel", sender: nil)
    }
    
    
  
    //segue function
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is StationTableVC{
            let nextController = (segue.destination as! StationTableVC)
            nextController.delegate = self
        }
    }
    
    fileprivate func setpView() {
        self.viwSchedule.alpha = 0
        self.btnTravel.alpha = 0
        self.btnSearchPlace.alpha = 0
        
        scheduleArr.append("5PM to 7PM")
        scheduleArr.append("8PM to 10PM")
        scheduleArr.append("6AM to 8APM")
        
        pickeView.delegate = self
        txtSchdule.inputView = pickeView
    }
    
    
    func validation() -> Bool {
        if ((txtArriving.text?.isEmpty)! || (txtDeparting.text?.isEmpty)!){
            return false
        }
        return true
    }
}

extension MainMenuVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.selectTextField = textField
        self.performSegue(withIdentifier: "table", sender: nil)
        textField.resignFirstResponder()
    }
}

extension MainMenuVC: StationTableVCDelegate{
    func selectStation(model: Station) {
        self.selectTextField.text = "\(model.stationCode) - \(model.stationName)"
        
        if (validation()){
            UIView.animate(withDuration: 1, animations: {
               self.viwSchedule.alpha = 1
            })
        }
    }
}

extension MainMenuVC: UIPickerViewDelegate , UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return scheduleArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return scheduleArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtSchdule.text = scheduleArr[row]
        txtSchdule.resignFirstResponder()
        
        UIView.animate(withDuration: 1, animations: {
            self.btnTravel.alpha = 1
            self.btnSearchPlace.alpha = 1
        })
    }
}
