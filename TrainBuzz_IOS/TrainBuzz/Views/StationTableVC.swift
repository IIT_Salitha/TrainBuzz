//
//  StationTableVC.swift
//  TrainBuzz
//
//  Created by Salitha on 5/12/18.
//  Copyright © 2018 Salitha. All rights reserved.
//

import UIKit
import Firebase

protocol StationTableVCDelegate {
    func selectStation(model: Station)
}

class StationTableVC: UIViewController {

    @IBOutlet weak var tblStations: UITableView!
    
    var stationsList:[Station] = []
    var ref: DatabaseReference?
    var dbHandle: DatabaseHandle = 0
    
    var delegate: StationTableVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        loadData()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func loadData() {
        
        ref = Database.database().reference()
        dbHandle = (ref?.child("Stations").observe(.childAdded, with: { (snapshot) in
            
            if let item = snapshot.value as? NSDictionary {
                let model = Station(stationCode: item.value(forKey: "stationCode") as! NSString , stationID: item.value(forKey: "stationID") as! NSNumber, stationName: item.value(forKey: "stationName") as! NSString)
                self.stationsList.append(model)
                self.tblStations.reloadData()
            }
        }))!
    }
    
    fileprivate func searchData(text: String) {
        
        self.stationsList = []
        
        ref = Database.database().reference()
        let strSearch = text.uppercased()
        ref?.child("Stations").queryOrdered(byChild:  "stationName").queryStarting(atValue: strSearch).queryEnding(atValue: strSearch + "\u{f8ff}").observeSingleEvent(of: .value, with: { (snapshot) in
    
            if let result = snapshot.value as? NSArray {
                
                for stationItem in result{
                    let item:NSDictionary  = stationItem as! NSDictionary
                    let model = Station(stationCode: item.value(forKey: "stationCode") as! NSString , stationID: item.value(forKey: "stationID") as! NSNumber, stationName: item.value(forKey: "stationName") as! NSString)
                    self.stationsList.append(model)
                }
            }
            else if let result = snapshot.value as? NSDictionary {
                for (key,_) in result {
                    let item:NSDictionary  = (result.value(forKey: key as! String) as? NSDictionary)!
                    let model = Station(stationCode: item.value(forKey: "stationCode") as! NSString , stationID: item.value(forKey: "stationID") as! NSNumber, stationName: item.value(forKey: "stationName") as! NSString)
                    self.stationsList.append(model)
                }
    
               
            }
            self.tblStations.reloadData()
        })
    }
    
    fileprivate func setUpView()  {
        self.tblStations.tableFooterView = UIView()
        self.tblStations.rowHeight = 44
        self.tblStations.estimatedRowHeight = UITableViewAutomaticDimension
        self.navigationItem.title = "Search"
    }
}

extension StationTableVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSection: Int = 0
        
        if stationsList.count > 0 {
            self.tblStations.backgroundView = nil
            numOfSection = 1
        } else {

            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tblStations.bounds.size.width, height: self.tblStations.bounds.size.height))
            noDataLabel.text = "No Data Available"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tblStations.backgroundView = noDataLabel
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:stationTC = tableView.dequeueReusableCell(withIdentifier: "cell") as! stationTC
        
        let stationModel = self.stationsList[indexPath.row]
        
        cell.lblName.text = stationModel.stationName as String
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let stationModel = self.stationsList[indexPath.row]
        delegate.selectStation(model: stationModel)
        _ = navigationController?.popViewController(animated: true)
    }
}

extension StationTableVC : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchData(text: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchData(text: searchBar.text ?? "A")
    }
}

struct Station {
    let stationCode: NSString
    let stationID: NSNumber
    let stationName: NSString
    
    init(stationCode: NSString, stationID:NSNumber, stationName: NSString) {
        self.stationCode = stationCode
        self.stationID = stationID
        self.stationName = stationName
    }
}
