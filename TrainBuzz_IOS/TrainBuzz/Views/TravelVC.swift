//
//  TravelVC.swift
//  TrainBuzz
//
//  Created by Salitha on 5/27/18.
//  Copyright © 2018 Salitha. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class TravelVC: TrainBuzzController {

    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    fileprivate func sendLocation (location:CLLocation) {
       
        let userLocation = [
            "from" : 1,
            "to" : 2,
            "username": "Roy",
            "location": "\(String(format: "%.4f", location.coordinate.latitude)),\(String(format: "%.4f", location.coordinate.longitude))",
            "speed": "\(String(format: "%.4f", location.speed * 1.61))",
            "timeStamp": 10
            ] as NSDictionary
        
        print(userLocation)
        let _ = TrainBuzzWebApi.PostRequest(userLocation, url: "userLoc/roy") { data, error in
            
            DispatchQueue.main.async(execute: {
                if let err = error {
                    self.displayConfirmMessage("Error", message: "Error occured. Please try again.", actionTitle: "OK", isPopOver: true)
                    print("Failed to get data from url:", err)
                    return
                    
                }
            })
        }
    }
    
}

extension TravelVC: GMSMapViewDelegate {
    
}

extension TravelVC: CLLocationManagerDelegate {
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newlocation = locations.last
        
        
        let camera = GMSCameraPosition.camera(withLatitude: (newlocation?.coordinate.latitude)!, longitude: (newlocation?.coordinate.longitude)!, zoom: 17.0)
        
        self.mapView?.animate(to: camera)
        
        if (newlocation?.speed)! * 1.0 > 0 {
            self.sendLocation(location: newlocation!)
        }
    }
}

struct userTrainLocationModal {
    let from: Int
    let to: Int
    let username: String
    let location: String
    let speed: String
    let timeStamp: Int
}
