import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RouterModule, Route} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatButtonModule, MatTableModule, MatProgressBarModule, MatFormFieldModule, MatInputModule, MatSelectModule,
        MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule, MatRadioModule, MatCardModule, MatMenuModule,
         MatIconModule, } from '@angular/material';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { environment } from '../environments/environment';
import { DeviceAllocationComponent } from './device-allocation/device-allocation.component';


const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'dashboard',
    component: DashboardComponent
  }
 ];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PushNotificationComponent,
    DeviceAllocationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebase), AngularFireDatabaseModule,
    FormsModule,
    ReactiveFormsModule ,
    BrowserAnimationsModule,
    MatButtonModule, MatInputModule, MatCardModule, MatMenuModule, MatIconModule, MatFormFieldModule, MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
