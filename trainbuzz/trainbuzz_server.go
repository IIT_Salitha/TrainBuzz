package main

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	//"strconv"
	"strings"
	//"sync"
	"github.com/codegangsta/negroni"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"time"
	//"reflect"
	"github.com/gorilla/mux"
	"trainbuzz/tb_firebase"
	"math"
	"strconv"
)

/*Constants
JSON Token Signer private Key file name and public key File Name
*/
const (
	privKeyPath = "app.rsa"
	pubKeyPath  = "app.rsa.pub"
)

/*Variable Declearation-Should optimize*/

var VerifyKeyB, SignKeyB []byte
var counter int
var index int
var (
	verifyKey  *rsa.PublicKey
	signKey    *rsa.PrivateKey
	serverPort int
)
var LastUpdatedTime int64
var LastRequestTime int64

/*   Stucts  Used By the package   */

type userCredentials struct {
	Username string `json:"Username"`
	Password string `json:"password"`
	Role     string `json:"role"`
	Email	 string `json:"email"`
}

type Response struct {
	Username     string `json:"username"`
	Computername string `json:"computername"`
}

/*Initial implementation sending Hash Map as an Slice -not Used in lastest Build*/
type JsonHashResponce struct {
	IP           string `json:"ip"`
	Username     string `json:"username"`
	Computername string `json:"computername"`
	Timestamp    int64  `json:"timeCreated"`
}

/*Json response to convert the User to Ip map as a json Reponse*/
type JsonMapResponce struct {
	Username     string `json:"username"`
	Computername string `json:"computername"`
	Timestamp    int64  `json:"timeCreated"`
}

/*stuct to send location of trains */
type JsonTrainlocationInfo struct {
	Id        string `json:"id"`
	Gps       string `json:"location"`
	Timestamp int64  `json:"timestamp"`
}

type trainLocation struct {
	gps       string
	timestamp int64
}

type JsonUserlocationInfo struct {
	From      int 	 `json:"from"`
	Gps       string `json:"location"`
	Speed  	  string	  `json:"speed"`
	Timestamp int64  `json:"timestamp"`
	To 		  int 	  `json:"to"`
	Username  string `json:"username"`
	
}
type TrainSpeed struct {
	Id 	string  `json:"id"`
	Speed   float64 `json:"speed"`
}

/*Json Token*/
type Token struct {
	Token string `json:"token"`
}

/*Signer Sample User Struct -Not Crucial*/
type CustomerInfo struct {
	Name string
	Kind string
}

/*Token Claim Signer Struct*/
type CustomClaimsExample struct {
	*jwt.StandardClaims
	TokenType string
	CustomerInfo
}

//machine infromation structure
type machineDetails struct {
	username     string
	computername string
	timestamp    int64
}
type ip_slice struct {
	Address string `json:"ip"`
}
type finalLocationTimeStamp struct {
	lat float64
	lon float64 
	time int64
}
/*map(collection) to save computer information*/
var user_to_IP_map map[string]machineDetails
var train_location map[string]trainLocation
var train_lastLocation map[string]finalLocationTimeStamp

/* map to save traininformation*/

var jsontoIP ip_slice

/*Create the Log file when server Starts */
func createLogFile() {
	if _, err := os.Stat("./trainbuzz.log"); os.IsNotExist(err) {
		os.Create("./trainbuzz.log")
	}

}
func check(e error) {
	if e != nil {
		panic(e)
	}
}

/*Get logFile from the system*/
func getLogFile() (*os.File, error) {
	logFile, err := os.OpenFile("./trainbuzz.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		createLogFile()
	}
	return logFile, err
}

/*Append a log to the log File */
func writeToLog(logInfo string) {
	logFile, _ := getLogFile()
	log.SetOutput(logFile)
	log.Printf(logInfo)
	logFile.Close()
}
func writeErrLogf(logInfo string, err error) {
	logFile, err1 := getLogFile()
	if err1 != nil {
		writeErrLogf("Can't find Log File", err1)
	}
	log.SetOutput(logFile)
	log.Printf(logInfo, err)
	logFile.Close()
}

/**
* initializng privatekey and publickey and converting it to JWT interface
* [err description]
* @type {[type]}
 */
func initKeys() {
	var err error
	user_to_IP_map = make(map[string]machineDetails)
	train_location = make(map[string]trainLocation)
	SignKeyB, err = ioutil.ReadFile(privKeyPath)
	if err != nil {
		writeToLog("Error reading private key ..Cannot Find Private Key Path\r")
		log.Fatal("Error reading private key")
		return
	}
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(SignKeyB)

	VerifyKeyB, err = ioutil.ReadFile(pubKeyPath)
	if err != nil {
		writeToLog("Error reading private key ..Cannot Find Public Key Path\r")
		log.Fatal("Error reading public key")
		return
	}
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(VerifyKeyB)

}


//Calculation Functions 
func init_lastTrainLocationMap() {
	train_lastLocation = make(map[string]finalLocationTimeStamp)
}

func hsin(theta float64) float64 {
		return math.Pow(math.Sin(theta/2), 2)
}

func calcSpeed(lat1, lon1, lat2, lon2 float64,time int64) float64{
	var speed float64
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100 // Earth radius in METERS

	// calculatess w 
	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)
	
	distance :=(2 * r * math.Asin(math.Sqrt(h)))
	speed =distance/float64(time)
	fmt.Println(distance)
	fmt.Println(speed)
	return speed
}	





func receiveTrainGps(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprintf(w, "Category: %v\n", vars["category"])
	writeToLog("Request Received for storing train location")
	var trainlocationJson JsonTrainlocationInfo
	var trainlocationMapValue trainLocation
	//Decode Request body
	err := json.NewDecoder(r.Body).Decode(&trainlocationJson)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Error in Request")
		writeErrLogf("Error in  Request,cannot Decode Train Information: %v\r,", err)
		return
	}
	trainlocationMapValue = trainLocation{gps: trainlocationJson.Gps, timestamp: trainlocationJson.Timestamp}
	//adding map
	train_location[trainlocationJson.Id] = trainlocationMapValue
	fmt.Println(trainlocationJson)
	fmt.Println(trainlocationJson.Id)
	fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
	child := fb.Child("TrainLocationns")
	newRef, err := child.Push(trainlocationJson)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("My new ref %s\n", newRef)
	fmt.Fprintf(w, "Details id gps time , %s %s %d", trainlocationJson.Id, trainlocationJson.Gps, trainlocationJson.Timestamp)
	logFile, err1 := getLogFile()
	if err1 != nil {
		writeErrLogf("Can't find Log File", err1)
	}
	log.SetOutput(logFile)
	log.Printf("Details id gps time , %s %s %d", trainlocationJson.Id, trainlocationJson.Gps, trainlocationJson.Timestamp)
	logFile.Close()
}

func receiveTrainGpsContextBased(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := fmt.Sprintf("TrainLocations/%v", vars["trainId"])
	var train_id string
	train_id = vars["trainId"]
	trainspeed_ref := fmt.Sprintf("TrainsSpeeds/%s", vars["trainId"])
	var time_diff int64
	switch r.Method {
	case "GET":

		fmt.Fprintf(w, "Category: %v\n", vars["trainId"])
		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		child := fb.Child(id)
		vb := child.OrderBy("id")
		var v interface{}
		v = "someshit"
		if err := vb.Value(&v); err != nil {
			fmt.Printf("error", err)

		}
		JsonResponse(v, w)

	case "POST":

		fmt.Fprintf(w, "Category: %v\n", vars["trainId"])
		writeToLog("Request Received for storing train location")
		var trainlocationJson JsonTrainlocationInfo
		var trainlocationMapValue trainLocation
		var lastTrainLocationTimeStamp  finalLocationTimeStamp
		var generalSpeed TrainSpeed
		//Decode Request body
		err := json.NewDecoder(r.Body).Decode(&trainlocationJson)
		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "Error in Request")
			writeErrLogf("Error in  Request,cannot Decode Train Information: %v\r,", err)
			return
		}
		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		trainlocationMapValue = trainLocation{gps: trainlocationJson.Gps, timestamp: trainlocationJson.Timestamp}
		//adding map
		train_location[trainlocationJson.Id] = trainlocationMapValue
		fmt.Println(trainlocationJson)
		fmt.Println(trainlocationJson.Id)
//Calculating SPEED OF THE  TRAIN
	    cordinates:= strings.Split(trainlocationJson.Gps,",")
		lat_r,_ :=strconv.ParseFloat(cordinates[0],64)
		lon_r,_:=strconv.ParseFloat(cordinates[1],64)
		lastTrainLocationTimeStamp =finalLocationTimeStamp{lat: lat_r,lon: lon_r,time:trainlocationJson.Timestamp}
		previousLocation,found :=train_lastLocation[train_id]
		if found {
			time_diff=(trainlocationJson.Timestamp) - (previousLocation.time)
			speed:= calcSpeed(previousLocation.lat,previousLocation.lon,lat_r,lon_r,time_diff)
			fmt.Println(speed)
			
			train_lastLocation[vars["trainId"]] = lastTrainLocationTimeStamp
			generalSpeed=TrainSpeed{Id:train_id,Speed:speed}
			//Adding the General speed
			fmt.Println(generalSpeed)
			speedChild :=fb.Child(trainspeed_ref)
			//ordered:=speedChild.OrderBy("id")
			
			
			generalSpeedRef :=speedChild.Update(generalSpeed)
		
			fmt.Printf("Speed new ref already Found %s\n", generalSpeedRef)
		}else{
			speedChild :=fb.Child(trainspeed_ref)
		
			//time_diff=trainlocationJson.Timestamp
			//speed:= calcSpeed(0,0,lat_r,lon_r,time_diff)
			generalSpeed=TrainSpeed{Id:train_id,Speed:0}
			generalSpeedRef :=speedChild.Update(generalSpeed)

		//	fmt.Println(speed)
			fmt.Println(generalSpeed)
			fmt.Printf("Speed new ref Not Found %s\n", generalSpeedRef)
			train_lastLocation[vars["trainId"]] = lastTrainLocationTimeStamp
			
		}
		
		
		fmt.Fprintf(w, "Details id gps time , %s %s %d", trainlocationJson.Id, trainlocationJson.Gps, trainlocationJson.Timestamp)
		child := fb.Child(id)
		newRef, err := child.Push(trainlocationJson)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Printf("My new ref %s\n", newRef)

	
		
		
		logFile, err1 := getLogFile()
		if err1 != nil {
			writeErrLogf("Can't find Log File", err1)
		}
		log.SetOutput(logFile)
		log.Printf("Details id gps time , %s %s %d", trainlocationJson.Id, trainlocationJson.Gps, trainlocationJson.Timestamp)
		logFile.Close()
	}
}

func receiveUserGps(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := fmt.Sprintf("UserLocations/%v", vars["category"])
	switch r.Method {
	case "GET":

		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		child := fb.Child(id)
		vb := child.OrderBy("id")
		var v interface{}
		v = "someshit"
		if err := vb.Value(&v); err != nil {
			fmt.Printf("error", err)

		}
		JsonResponse(v, w)
	case "POST":
		writeToLog("Request Received for storing train location")
		var userJson JsonUserlocationInfo
		var trainlocationMapValue trainLocation
		//Decode Request body
		err := json.NewDecoder(r.Body).Decode(&userJson)
		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "Error in Request")
			writeErrLogf("Error in Login Request,cannot Decode User Login Information: %v\r,", err)
			return
		}
		trainlocationMapValue = trainLocation{gps: userJson.Gps, timestamp: userJson.Timestamp}
		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		child := fb.Child("UserLocations")
		newRef, err := child.Push(userJson)
		fmt.Printf("User new ref %s\n", newRef)
		train_location[userJson.Username] = trainlocationMapValue
	}

}

func receiveUserGpsContextBased(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := fmt.Sprintf("UserLocation/%v", vars["userId"])
	switch r.Method {
	case "GET":
		fmt.Fprintf(w, "userId: %v\n", vars["userId"])
		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		child := fb.Child(id)
		vb := child.OrderBy("username")
		var v interface{}
		v = "someshit"
		if err := vb.Value(&v); err != nil {
			fmt.Printf("error", err)

		}
		JsonResponse(v, w)
	case "POST":

		writeToLog("Request Received for storing train location")
		var userJson JsonUserlocationInfo
		
		//Decode Request body
		err := json.NewDecoder(r.Body).Decode(&userJson)
		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "Error in Request")
			writeErrLogf("Error in Login Request,cannot Decode User Login Information: %v\r,", err)
			return
		}
		fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
		child := fb.Child(id)
		newRef, err := child.Push(userJson)
		fmt.Printf("User new ref %s\n", newRef)
		
	}
}

/**
* autheticating user through request using JSON token
* [err description]
* @type {[type]}

 */
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	writeToLog("Login request Identified\r")
	var user userCredentials
	var v []map[string]interface{}
	var nameAuthenticated bool
	var passAuthenticated bool
	//var userM map[string]interface{}
	//Decode Request body
	//err := json.NewDecoder(r.Body).Decode(&user)
	err := r.ParseForm()
	//fmt.Println(r.Form)
	//	fmt.Println("path", r.URL.Path)
	//fmt.Println("scheme", r.URL.Scheme)
	//fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		//fmt.Println("key:", k)
		//fmt.Println("val:", strings.Join(v, ""))
		if k == "username" {
			user.Username = strings.Join(v, "")
			fmt.Println(user.Username)
		} else if k == "password" {
			user.Password = strings.Join(v, "")
			fmt.Println(user.Password)
		} else {
			user.Role = strings.Join(v, "")
		}
	}
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Error in Request")
		writeToLog("Error in Login Request,cannot Decode User Login Information\r")
		return
	}

	fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
	child := fb.Child("Users")
	vb := child.OrderBy("Username")

	if err := vb.Value(&v); err != nil {
		fmt.Printf("error", err)

	}
	//jsonUsers, err := json.Marshal(v)
	//err = json.Unmarshal(jsonUsers, &userM)
	for _, userMaps := range v {
		nameAuthenticated = false
		passAuthenticated = false
		for _, value := range userMaps {

			if value == user.Username {
				nameAuthenticated = true
				continue
			} else if value == user.Password {
				passAuthenticated = true
			}
			if passAuthenticated && nameAuthenticated {
				//create a rsa 256 Signer
				signer := jwt.New(jwt.GetSigningMethod("RS256"))
				//setting up token claims---------------------------- check neigro
				signer.Claims = &CustomClaimsExample{
					&jwt.StandardClaims{
						// setting the  the expire time
						ExpiresAt: time.Now().Add(time.Minute * 60 * 24 * 7).Unix(),
					},
					"level1",
					CustomerInfo{"test", "human"},
				}
				tokenString, err := signer.SignedString(signKey)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintln(w, "Error while signing the token")
					writeErrLogf("Error signing token: %v\r,", err)

				}
				response := Token{tokenString}
				JsonResponse(response, w)
				return
			} else {
				w.WriteHeader(http.StatusBadRequest)
				writeToLog("Error in Login Request,cannot Invalid Password\r")
				fmt.Fprint(w, "Invalid credentials")
				return
			}
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprint(w, "Invalid credentials username weasnt found")

}

/**
 *validated the Token as a middleware
 * If valdiated it will use the wrapped next handler ------> ProtectedHandler in this case
 * [err description]
 * @type {[type]}
 */
func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	writeToLog("Validation the Request Token for a Protected Resource")
	//validate token
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &CustomClaimsExample{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	if err == nil {

		if token.Valid {
			next(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, "Token is not valid")
			writeToLog("Attempt to Get Protected Information with an invalid Token\r")
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		//  log.Println("checking if its not empty at Token valid:",r.Body)
		fmt.Fprint(w, "Unauthorised access to this resource")
		writeErrLogf("Attempt to Get Protected Information with an invalid Token: %v \r", err)
	}
}

/**
 * creating a Response by converting a string to JSON(Not Required)
 * [err description]
 * @type {[type]}
 */
func JsonResponse(response interface{}, w http.ResponseWriter) {

	json1, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(json1)
}

/**
 * creating a Response to send a dunamic GO SLICE as a JSON Response
 * [err description]
 * @type {[type]}
 */
func JsonSliceResponse(response interface{}, w http.ResponseWriter) {

	json1, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var unmarshalReponsedArray string
	jsonBytetoString := string(json1)
	withoutBrackets := strings.Trim(jsonBytetoString, "[]")
	withoutbackSlashes := strings.Trim(withoutBrackets, "\\")

	json2, err2 := json.Marshal(withoutbackSlashes)
	json.Unmarshal(json2, &unmarshalReponsedArray)
	if err2 != nil {
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}
	json3, _ := json.Marshal(unmarshalReponsedArray)
	log.Println("json:", json2)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(json3)
}

/**
 * Secured Content through authetication
 * [response description]
 * @type {[type]}
 */
func ProtectedHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "testdata.txt")
	//response := Response{"Gained access to protected resource"}
	//  JsonResponse(response, w)

}

/**
 * function will recived the IP as request and assign it to jsontoIP struct globally
 * @type {[type]}
 */
func requestor(w http.ResponseWriter, r *http.Request) {
	err := json.NewDecoder(r.Body).Decode(&jsontoIP)
	log.Println("checking if its not emptysd:", jsontoIP)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Error in Request")
		return
	}

}

/*
function to request information about an IP
*/

func sendUsertoIPInfo(w http.ResponseWriter, r *http.Request) {
	writeToLog("Request to GET User to IP Tables Detected\r")
	var responseMapData JsonMapResponce
	//IF this is the First Request Send the Entire Map as a Response
	if LastRequestTime == 0 {
		var responseMap map[string]JsonMapResponce
		responseMap = make(map[string]JsonMapResponce)
		LastRequestTime = time.Now().Unix()
		//responseArray = make([]JsonHashResponce, 1)
		writeToLog("RequestTime\r")
		fmt.Println(LastRequestTime)
		//traverse map and create ResponseHashMap Temp -Can be optimized just by converting the UsertoIp map to JSON
		for key, value := range user_to_IP_map {
			responseMapData = JsonMapResponce{Username: value.username, Computername: value.computername, Timestamp: value.timestamp}
			responseMap[key] = responseMapData

		}
		//Sending the Response
		writeToLog("Entire Table Sent to Client\r")
		JsonResponse(responseMap, w)

	} else {
		var responseMap map[string]JsonMapResponce
		responseMap = make(map[string]JsonMapResponce)
		LastRequestTime = time.Now().Unix()
		for key, value := range user_to_IP_map {
			if value.timestamp > LastUpdatedTime {

				responseMapData = JsonMapResponce{Username: value.username, Computername: value.computername, Timestamp: value.timestamp}
				responseMap[key] = responseMapData
			}

		}
		//Sending Response
		writeToLog("Updated Content is sent to the User\r")
		JsonResponse(responseMap, w)
	}
}


func welcome(w http.ResponseWriter, r *http.Request) {

	// write data to response
	//dat, err := ioutil.ReadFile("./trainbuzz.log")
	//check(err)
	//fmt.Fprintf(w,"does this woork")
	t, _ := template.ParseFiles("./trainbuzz.log")
	t.Execute(w, nil)
	//fmt.Fprintf(w,string(dat))

}

/*func registrationHandler(w http.ResponseWriter, r *http.Request){
	var userJson userCredentials
	err := json.NewDecoder(r.Body).Decode(&userJson)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Error in Request")
		writeErrLogf("Error in  Request,cannot Decode Train Information: %v\r,", err)
		return
	}
	auth := smtp.PlainAuth("", "user@example.com", "password", "mail.example.com")

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{"recipient@example.net"}
	msg := []byte("To: recipient@example.net\r\n" +
		"Subject: discount Gophers!\r\n" +
		"\r\n" +
		"This is the email body.\r\n")
	err := smtp.SendMail("mail.example.com:25", auth, "sender@example.org", to, msg)
	if err != nil {
		log.Fatal(err)
	}

}
*/
//Get All Trainstation through firebase
func getTrainStations(w http.ResponseWriter, r *http.Request) {
	fb := tb_firebase.FireBaseNewConnection("https://trackerx-9d58e.firebaseio.com/")
	child := fb.Child("Stations")
	vb := child.OrderBy("stationName")
	var v interface{}
	v = "someshit"
	if err := vb.Value(&v); err != nil {
		fmt.Printf("error", err)

	}

	//println(fb.url)
	//var v interface{}
	//if err := fb.Value(v); err != nil {
	//	println("some error")
	//	writeErrLogf("error on staions",err)
	//
	//	fmt.Fprintf(w,"this aint working")
	//}

	//log.Printf("My value %v\n", v)
	JsonResponse(v, w)
}

/**
 * starting the server
 * need ot
understand negroni
 * [mySigningKey description]
 * @type {Array}

*/
func StartServer() {
	index = 0
	//Creating logFilefa
	createLogFile()
	init_lastTrainLocationMap()
	r := mux.NewRouter()
	//PUBLIC ENDPOINTS``
	r.HandleFunc("/trainLoc/{trainId}", receiveTrainGpsContextBased)
	//r.HandleFunc("/userLoc/{userId}", receiveUserGps)
	r.HandleFunc("/token", LoginHandler)
	r.HandleFunc("/", welcome)
	r.HandleFunc("/stations", getTrainStations)
	r.Handle("/userLoc/{userId}",negroni.New(
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(receiveUserGpsContextBased)),
	))
	http.Handle("/", r)
	//http.HandleFunc("/token", LoginHandler)
	//http.HandleFunc("/", welcome)
	//http.HandleFunc("/trainLoc", receiveTrainGps)
	//http.HandleFunc("/stations", getTrainStations)
	//http.HandleFunc("/loginldap", loginLdap)
	//http.HandleFunc("/request", requestor)
	//http.HandleFunc("/ldap", adHandler)
	//http.HandleFunc("/dhcp", dhcp_client_receiver)
	//PROTECTED ENDPOINTS

	//http.Handle("/trainLoc/", negroni.New(
	//	negroni.HandlerFunc(ValidateTokenMiddleware),
	//	negroni.Wrap(http.HandlerFunc(receiveTrainGps)),
	//))
	
	//http.Handle("/userLoc/", negroni.New(
	//	negroni.HandlerFunc(ValidateTokenMiddleware),
	//	negroni.Wrap(http.HandlerFunc(receiveUserGps)),
	//))

	writeToLog("Starting Server at port 8000\r")
	//err := http.ListenAndServeTLS(":8000", "hostcert.pem", "hostkey.pem", nil)
	//err := http.ListenAndServe(":8080", nil)
	//	if err != nil {
	//	fmt.Println(err)
	//writeErrLogf("ListenAndServe: %v\r", err)
	//}
	log.Fatal(http.ListenAndServe(":8000", nil))

}

func main() {

	initKeys()
	StartServer()

}
