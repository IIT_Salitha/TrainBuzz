package tb_firebase

import (
	//"fmt"
	////"ime"
	"github.com/zabawaba99/firego"
)

//type  Tb_firebase struct {
//url string

//}

var authObj firego.Firebase

func Firebase_Auth(url string) {
	fb := firego.New(url, nil)
	fb.Auth("my-token")
}

func FireBaseNewConnection(url string) *firego.Firebase {
	fb := firego.New(url, nil)

	return fb
}

func Firebase_Child(url string, fb firego.Firebase) *firego.Firebase {

	childFB := fb.Child(url)
	return childFB

}

func Firebase_GetStation(url string, fb firego.Firebase) *firego.Firebase {

	// Set value
	fbOut := fb.OrderBy("stationName")
	// Remove query parameter
	//fbOut = fb.StartAt("")
	return fbOut
}
