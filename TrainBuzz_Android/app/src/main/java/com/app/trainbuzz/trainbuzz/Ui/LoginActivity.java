package com.app.trainbuzz.trainbuzz.Ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.trainbuzz.trainbuzz.Models.AuthorizedUser;
import com.app.trainbuzz.trainbuzz.R;
import com.app.trainbuzz.trainbuzz.Service.RetrofitFractory;
import com.app.trainbuzz.trainbuzz.Service.TrainBuzzAPI;
import com.app.trainbuzz.trainbuzz.helpers.AlertDialogHelper;
import com.app.trainbuzz.trainbuzz.helpers.SharedPreferencesHelper;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity /*implements View.OnClickListener */ {

    private EditText txtUsername, txtPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    private void authorize(String username, String password) {

        RetrofitFractory.getRetrofitRx().create(TrainBuzzAPI.class)
                .AuthorizeUser(username, password, "password")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AuthorizedUser>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(AuthorizedUser authorizedUser) {

                        SharedPreferencesHelper.setEmailPreference(getApplicationContext(), txtUsername.getText().toString().trim());
                        SharedPreferencesHelper.setPasswordPreference(getApplicationContext(), txtPassword.getText().toString().trim());
                        SharedPreferencesHelper.setTokenPreference(getApplicationContext(), authorizedUser.getToken());
                        SharedPreferencesHelper.setLoginPreferances(getApplicationContext(), true);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof IOException) {
                            AlertDialogHelper.
                                    showNetworkErrorDialog(LoginActivity.this, "Retry", () -> authorize(txtUsername.getText().toString().trim(), txtPassword.getText().toString().trim()), true, null, null);
                        } else if (e instanceof HttpException) {

                            HttpException httpException = ((HttpException) e);
                            switch (httpException.code()) {
                                case 400:
                                    AlertDialogHelper.showMessageDialog(LoginActivity.this, "Login failed", "Check username or password", null);
                                    break;
                                default:
                                    AlertDialogHelper.showErrorMessageDialog(LoginActivity.this, null);
                                    break;
                            }
                        } else {
                            AlertDialogHelper.showErrorMessageDialog(LoginActivity.this, null);
                        }
                    }
                });
    }

    public void login(View view) {

        if (!(txtUsername.getText().toString().trim().isEmpty()) || !(txtPassword.getText().toString().trim().isEmpty())) {
            authorize(txtUsername.getText().toString().trim(), txtPassword.getText().toString().trim());
        } else {
            Toast.makeText(this, "Fill your details to login ", Toast.LENGTH_SHORT).show();
        }
    }
}
