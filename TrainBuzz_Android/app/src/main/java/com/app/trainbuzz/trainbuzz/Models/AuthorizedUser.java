package com.app.trainbuzz.trainbuzz.Models;

public class AuthorizedUser {

    private String token;

    public AuthorizedUser(){

    }

    public AuthorizedUser(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
