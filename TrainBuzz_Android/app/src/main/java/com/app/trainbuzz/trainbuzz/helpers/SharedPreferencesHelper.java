package com.app.trainbuzz.trainbuzz.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by android-dev on 10/6/17.
 */

public class SharedPreferencesHelper {
    private static SharedPreferences sharedPreferences;
    private static String mySharePreference = "myPreference";
    private static String keyUsername = "username";
    private static String keyPassword = "password";
    private static String keyToken = "token";
    private static String keyName = "companyName";
    private static String keyLogin = "login";
    private static String keyUserImage = "image";
    private static String keyUsual = "usual";

    private SharedPreferencesHelper() {

    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(mySharePreference, Context.MODE_PRIVATE);
            return sharedPreferences;
        } else {
            return sharedPreferences;
        }
    }

    public static boolean clearSharedPreferences(Context context) {

        return getSharedPreferences(context).edit().clear().commit();
    }

    public static void setEmailPreference(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(keyUsername, email);
        editor.apply();
    }

    public static String getEmailPreference(Context context) {
        return getSharedPreferences(context).getString(keyUsername, "");
    }

    public static void setPasswordPreference(Context context, String password) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(keyPassword, password);
        editor.apply();
    }

    public static String getPasswordPreference(Context context) {
        return getSharedPreferences(context).getString(keyPassword, " ");
    }

    public static void setTokenPreference(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        String bearerToken = "bearer " + token;
        editor.putString(keyToken, bearerToken);
        editor.apply();
    }

    public static String getTokenPreference(Context context) {
        return getSharedPreferences(context).getString(keyToken, "");
    }

    public static void setNamePreferances(Context context, String firstName) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(keyName, firstName);
        editor.apply();
    }

    public static String getNamePreferences(Context context) {
        return getSharedPreferences(context).getString(keyName, "");
    }

    public static void setImagePreferances(Context context, String imagePath) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(keyUserImage, imagePath);
        editor.apply();
    }

    public static String getImagePreferences(Context context) {
        return getSharedPreferences(context).getString(keyUserImage, "");
    }

    public static void setLoginPreferances(Context context, Boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(keyLogin, status);
        editor.apply();
    }

    public static boolean getLoginPreferences(Context context) {
        return getSharedPreferences(context).getBoolean(keyLogin, false);
    }

    public static void setUsualPreferances(Context context, String usual) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(keyUsual, usual);
        editor.apply();
    }

    public static String getUsualPreferences(Context context) {
        return getSharedPreferences(context).getString(keyUsual, "");
    }

}
