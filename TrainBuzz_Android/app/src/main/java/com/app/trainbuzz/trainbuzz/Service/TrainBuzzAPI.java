package com.app.trainbuzz.trainbuzz.Service;

import com.app.trainbuzz.trainbuzz.Models.AuthorizedUser;
import com.app.trainbuzz.trainbuzz.Models.UserLocation;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface TrainBuzzAPI {

    @FormUrlEncoded
    @POST("/token")
    Single<AuthorizedUser> AuthorizeUser(@Field("username") String username,
                                         @Field("password") String password,
                                         @Field("grant_type") String grant_type);

    @POST("/userLoc/roy")
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    Single<ResponseBody> sendUserLocation(@Header("Authorization") String token,
                                          @Body UserLocation userLocation);
}
