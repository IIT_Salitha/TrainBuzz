package com.app.trainbuzz.trainbuzz.Ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.app.trainbuzz.trainbuzz.Adapters.StationList;
import com.app.trainbuzz.trainbuzz.Models.Station;
import com.app.trainbuzz.trainbuzz.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class StationListActivity extends AppCompatActivity {

    DatabaseReference databaseReference;

    ListView listViewStation;
    EditText searchText;

    List<Station> stationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_list);

        listViewStation = findViewById(R.id.listViewStation);
        searchText = findViewById(R.id.searchTxt);

        databaseReference = FirebaseDatabase.getInstance().getReference("Stations");

        stationList = new ArrayList<>();

        listViewStation.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent();
            intent.putExtra("selectStationModel", stationList.get(position));
            setResult(RESULT_OK, intent);
            finish();
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!s.toString().isEmpty()){
                    searchStation(searchText.getText().toString().toUpperCase());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseReference.orderByKey().limitToFirst(50).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                populateStationList(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void searchStation(String text){

        databaseReference.orderByChild("stationName").startAt(text).endAt(text + "\\u{f8ff}").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot);
                populateStationList(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void populateStationList(DataSnapshot dataSnapshot) {
        stationList.clear();
        for (DataSnapshot stationSnapshot: dataSnapshot.getChildren()) {
            Station station = stationSnapshot.getValue(Station.class);
            stationList.add(station);

        }

        StationList adapter = new StationList(StationListActivity.this, stationList);
        listViewStation.setAdapter(adapter);
    }
}
