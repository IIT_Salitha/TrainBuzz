package com.app.trainbuzz.trainbuzz.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.app.trainbuzz.trainbuzz.Models.AuthorizedUser;
import com.app.trainbuzz.trainbuzz.R;
import com.app.trainbuzz.trainbuzz.Service.RetrofitFractory;
import com.app.trainbuzz.trainbuzz.Service.TrainBuzzAPI;
import com.app.trainbuzz.trainbuzz.helpers.AlertDialogHelper;
import com.app.trainbuzz.trainbuzz.helpers.SharedPreferencesHelper;

import java.io.IOException;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LandActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);
    }

    @Override
    protected void onResume() {

        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreferencesHelper.getLoginPreferences(LandActivity.this)) {

                    String usernamePreference = SharedPreferencesHelper.getEmailPreference(LandActivity.this);
                    String passwordPreference = SharedPreferencesHelper.getPasswordPreference(LandActivity.this);
                    authorize(usernamePreference, passwordPreference);
                } else {
                    redirectToLogin();
                }
            }
        }, 300);
    }

    private void redirectToLogin() {
        startActivity(new Intent(LandActivity.this, LoginActivity.class));
    }

    private void authorize(final String username, final String password) {

        RetrofitFractory.getRetrofitRx().create(TrainBuzzAPI.class)
                .AuthorizeUser(username, password, "password")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AuthorizedUser>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(AuthorizedUser authorizedUser) {

                        SharedPreferencesHelper.setEmailPreference(getApplicationContext(), username);
                        SharedPreferencesHelper.setPasswordPreference(getApplicationContext(), password);
                        SharedPreferencesHelper.setTokenPreference(getApplicationContext(), authorizedUser.getToken());
                        SharedPreferencesHelper.setLoginPreferances(getApplicationContext(), true);
                        startActivity(new Intent(LandActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof IOException) {
                            AlertDialogHelper.
                                    showNetworkErrorDialog(LandActivity.this, null, () -> authorize(username, password), true, null, null);
                        } else {
                            redirectToLogin();
                        }
                    }
                });

    }
}
