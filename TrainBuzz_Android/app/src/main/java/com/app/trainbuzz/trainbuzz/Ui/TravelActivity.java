package com.app.trainbuzz.trainbuzz.Ui;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.app.trainbuzz.trainbuzz.GPS_Service;
import com.app.trainbuzz.trainbuzz.Models.UserLocation;
import com.app.trainbuzz.trainbuzz.R;
import com.app.trainbuzz.trainbuzz.Service.RetrofitFractory;
import com.app.trainbuzz.trainbuzz.Service.TrainBuzzAPI;
import com.app.trainbuzz.trainbuzz.helpers.SharedPreferencesHelper;
import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import com.github.anastr.speedviewlib.SpeedView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static android.support.v4.app.NotificationCompat.*;


public class TravelActivity extends AppCompatActivity
        implements
        OnMapReadyCallback,
        LocationListener, GoogleMap.OnInfoWindowClickListener{

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;

    private GoogleMap mMap;
    private LocationManager locationManager;
    private BroadcastReceiver broadcastReceiver;

    AwesomeSpeedometer speedometer;
    TextView txtDebug;
    Builder mBuilder;


    private void showMyLocation(Location location) {
        LatLng mylocation = new LatLng(location.getLatitude(), location.getLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(mylocation)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void postUserLocation(Location location,Double speed) {

        UserLocation userLocation = new UserLocation(1,2,"Roy", Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude()),Double.toString(speed),10);


        RetrofitFractory.getRetrofitRx().create(TrainBuzzAPI.class)
                .sendUserLocation(SharedPreferencesHelper.getTokenPreference(TravelActivity.this), userLocation)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        speedometer  = (AwesomeSpeedometer) findViewById(R.id.speedView);


        speedometer.setIndicatorWidth(50 );

    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onResume() {
        super.onResume();

        if (broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    double current_speed =  (Double)intent.getExtras().get("speed");
                    Location current_location = (Location) intent.getExtras().get("location");
                    showMyLocation(current_location);
                    speedometer.speedTo((float)current_speed);
                    postUserLocation(current_location, current_speed);
                    newNotification(current_speed);

                }
            };
        }

        registerReceiver(broadcastReceiver,new IntentFilter("location update"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.locationManager.removeUpdates(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        initializeLocationManager();
    }

    private void initializeLocationManager() {

        //get the location manager
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //define the location manager criteria
        Criteria criteria = new Criteria();
        String locationProvider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        if (!locationManager.isProviderEnabled(locationProvider)) {

        }

        this.locationManager.requestLocationUpdates(locationProvider, 10, 1, this);
        Location location = locationManager.getLastKnownLocation(locationProvider);
        mMap.setMyLocationEnabled(true);

        //initialize the location
        if (location != null) {
            Intent intent = new Intent(getApplicationContext(), GPS_Service.class);
            startService(intent);

            //onLocationChanged(location);
        }
    }

    private void newNotification(double speed){

        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Trainbuzz")
                .setContentText("We buzzing your train. Current speed " + (int) speed + "Kmph")
                .setAutoCancel(true)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

    }

    @Override
    public void onLocationChanged(Location location) {

        showMyLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        System.out.println("on ");
    }

    @Override
    public void onProviderDisabled(String provider) {
        System.out.println("on ");

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }
}