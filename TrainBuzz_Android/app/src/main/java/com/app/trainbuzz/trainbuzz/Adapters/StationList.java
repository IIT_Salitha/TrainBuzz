package com.app.trainbuzz.trainbuzz.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.trainbuzz.trainbuzz.Models.Station;
import com.app.trainbuzz.trainbuzz.R;

import java.util.List;

public class StationList extends ArrayAdapter<Station>{

    private Activity context;
    private List<Station> stationList;

    public StationList (Activity context , List<Station>stationList){
        super(context, R.layout.station_list_layout, stationList);
        this.context = context;
        this.stationList = stationList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.station_list_layout,null,true);

        TextView txtViewName = (TextView) listViewItem.findViewById(R.id.txtViwName);

        Station station = stationList.get(position);
        txtViewName.setText(station.getStationName());

        return listViewItem;
    }
}
