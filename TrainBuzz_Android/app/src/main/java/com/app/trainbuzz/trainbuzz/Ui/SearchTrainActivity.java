package com.app.trainbuzz.trainbuzz.Ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.trainbuzz.trainbuzz.Models.UserLocation;
import com.app.trainbuzz.trainbuzz.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SearchTrainActivity extends AppCompatActivity  implements
        OnMapReadyCallback,
        LocationListener, GoogleMap.OnInfoWindowClickListener {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private String locationProvider;

    private boolean isFirstCall;
    private Location mLastLocation;
    private Location lastLocation;

    List<UserLocation> locationList;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_train);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("UserLocation").child("roy");
        getMapData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        this.locationManager.removeUpdates(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        initializeLocationManager();
    }

    private void initializeLocationManager() {

        //get the location manager
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //define the location manager criteria
        Criteria criteria = new Criteria();
        this.locationProvider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        if (!locationManager.isProviderEnabled(locationProvider)) {

        }

        this.locationManager.requestLocationUpdates(this.locationProvider, 10, 1, this);
        Location location = locationManager.getLastKnownLocation(locationProvider);
        mMap.setMyLocationEnabled(true);
        //initialize the location
        if (location != null) {

            onLocationChanged(location);
        }
    }

   private void getMapData(){

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //System.out.println(dataSnapshot.getValue(UserLocation.class));

                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    UserLocation userLocation = ds.getValue(UserLocation.class);
                    System.out.println(userLocation);
                    locationList.add(userLocation);
                    addMarker(locationList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
   }

   private void addMarker(List<UserLocation> locations){

        mMap.clear();

        if (locations.size() < 1){
            return;
        }

        String[] loc = locations.get(0).getLocation().split(",");
        double lat = Double.parseDouble(loc[0]);
        double lan = Double.parseDouble(loc[1]);

       LatLng firstLocation = new LatLng(lat, lan);
       mMap.addMarker(new MarkerOptions().position(firstLocation)
               .title("Start Location"));

       loc = locations.get(locations.size()-1).getLocation().split(",");
       lat = Double.parseDouble(loc[0]);
       lan = Double.parseDouble(loc[1]);

       LatLng lastLocation = new LatLng(lat, lan);
       mMap.addMarker(new MarkerOptions().position(lastLocation)
               .title("Current Location"));

       CameraPosition cameraPosition = new CameraPosition.Builder()
               .target(lastLocation)      // Sets the center of the map to Mountain View
               .zoom(17)                   // Sets the zoom
               .bearing(90)                // Sets the orientation of the camera to east
               .tilt(30)                   // Sets the tilt of the camera to 30 degrees
               .build();                   // Creates a CameraPosition from the builder
       mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

       PolylineOptions polylineOptions = new PolylineOptions().
               geodesic(true).
               color(Color.RED).
               width(10);

       for (int i = 0; i < locations.size(); i++) {
           loc = locations.get(i).getLocation().split(",");
           lat = Double.parseDouble(loc[0]);
           lan = Double.parseDouble(loc[1]);
           polylineOptions.add(new LatLng(lat, lan));
       }

       mMap.addPolyline(polylineOptions);
   }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }
}
