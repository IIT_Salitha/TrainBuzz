package com.app.trainbuzz.trainbuzz;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

public class GPS_Service extends Service {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location mLastLocation;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                double speed = 0;
                if (mLastLocation != null)
                    speed = Math.sqrt(
                            Math.pow(location.getLongitude() - mLastLocation.getLongitude(), 2)
                                    + Math.pow(location.getLatitude() - mLastLocation.getLatitude(), 2)
                    ) / (location.getTime() - mLastLocation.getTime());

                if (location.hasSpeed())
                    speed = location.getSpeed();

                mLastLocation = location;


                speed = ((speed * 3600) / 1000);

                if (speed > 0 && mLastLocation.hasAccuracy()) {
                    Intent intent = new Intent("location update");
                    intent.putExtra("location",location);
                    intent.putExtra("speed", speed);
                    sendBroadcast(intent);
                }

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, locationListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(locationManager != null){
            locationManager.removeUpdates(locationListener);
        }
    }
}
