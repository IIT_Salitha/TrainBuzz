package com.app.trainbuzz.trainbuzz.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

/**
 * Created by Android Developer on 3/29/2018.
 */

public class AlertDialogHelper {

    public static void showNetworkErrorDialog
            (@NonNull Context context, @Nullable String positiveText, @Nullable Runnable positiveRunnable, boolean hasNegativeButton, @Nullable String negativeText, @Nullable Runnable negativeRunnable) {

        createDialog(context,
                false,
                "Network error",
                "Could not connect to the server. please check your internet connection and retry.",
                true,
                positiveText != null ? positiveText : "Retry", positiveRunnable, hasNegativeButton, negativeText != null ? negativeText : "Cancel", negativeRunnable);
    }

    public static void showErrorMessageDialog
            (@NonNull Context context, @Nullable Runnable positiveRunnable) {

        createDialog(context,
                false,
                "Error occurred",
                "Unexpected error occurred. please try later.",
                true,
                "Ok", positiveRunnable, false, "", null);

    }

    public static void showMessageDialog
            (@NonNull Context context, @NonNull String title, @NonNull String message, @Nullable Runnable positiveRunnable) {

        createDialog(context,
                false,
                title,
                message,
                true,
                "Ok", positiveRunnable, false, "", null);

    }

    public static void showConfirmDialog
            (@NonNull Context context, @NonNull String title, @NonNull String message, @Nullable Runnable positiveRunnable, @Nullable Runnable negativeRunnable) {

        createDialog(context,
                false,
                title,
                message,
                true,
                "Yes", positiveRunnable, true, "No", negativeRunnable);

    }


    private static void createDialog
            (@NonNull Context context, boolean cancelable, @NonNull String title, @NonNull String message, boolean hasPositiveButton,
             @NonNull String positiveText, @Nullable Runnable positiveRunnable, boolean hasNegativeButton, @NonNull String negativeText, @Nullable Runnable negativeRunnable) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setCancelable(cancelable)
                .setTitle(title)
                .setMessage(message);

        if (hasPositiveButton) {
            builder.setPositiveButton(positiveText, (dialogInterface, i) -> {
                if (positiveRunnable != null) {
                    positiveRunnable.run();
                }
            });
        }

        if (hasNegativeButton) {
            builder.setNegativeButton(negativeText, (dialogInterface, i) -> {
                if (negativeRunnable != null) {
                    negativeRunnable.run();
                }
            });
        }
        alertDialog = builder.create();
        alertDialog.show();
    }
}
