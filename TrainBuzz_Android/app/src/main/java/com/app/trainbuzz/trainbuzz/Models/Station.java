package com.app.trainbuzz.trainbuzz.Models;

import java.io.Serializable;

public class Station implements Serializable {

    private Integer stationID;
    private String stationCode;
    private String stationName;

    public  Station(){

    }

    public Station(Integer stationID, String stationCode, String stationName) {
        this.stationID = stationID;
        this.stationCode = stationCode;
        this.stationName = stationName;
    }

    public Integer getStationID() {
        return stationID;
    }

    public String getStationCode() {
        return stationCode;
    }


    public String getStationName() {
        return stationName;
    }

}
