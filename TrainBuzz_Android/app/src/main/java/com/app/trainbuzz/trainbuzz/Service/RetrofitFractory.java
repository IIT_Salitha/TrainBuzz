package com.app.trainbuzz.trainbuzz.Service;

import com.app.trainbuzz.trainbuzz.BuildConfig;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFractory {

    private static Retrofit retrofitRx = null;

    public static Retrofit getRetrofitRx() {

        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (BuildConfig.DEBUG) {
            okhttpClientBuilder.addInterceptor(logging);
        }

        if (retrofitRx == null) {
            retrofitRx = new Retrofit.Builder()
                    .baseUrl("http://35.200.178.94:8000/")
                    .client(okhttpClientBuilder.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitRx;
    }


}
