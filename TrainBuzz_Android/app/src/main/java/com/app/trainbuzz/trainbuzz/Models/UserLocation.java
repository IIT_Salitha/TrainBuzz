package com.app.trainbuzz.trainbuzz.Models;

public class UserLocation {

    public Integer from;
    public Integer to;
    public String username;
    public String location;
    public String speed;
    public Integer timestamp;

    public UserLocation() {
    }

    public UserLocation(Integer from, Integer to, String username, String location, String speed, Integer timestamp) {
        this.from = from;
        this.to = to;
        this.username = username;
        this.location = location;
        this.speed = speed;
        this.timestamp = timestamp;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "UserLocation{" +
                "from=" + from +
                ", to=" + to +
                ", userName='" + username + '\'' +
                ", location='" + location + '\'' +
                ", speed='" + speed + '\'' +
                ", timeStamp=" + timestamp +
                '}';
    }
}
