package com.app.trainbuzz.trainbuzz.Ui;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.trainbuzz.trainbuzz.Models.Station;
import com.app.trainbuzz.trainbuzz.R;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

     TextView txtArriving;
     TextView txtDeparting;
     LinearLayout scheduleViw;
     Spinner scheduleDropdown;
     Button btnTravel, btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTravel =  findViewById(R.id.btnTravel);
        btnSearch =  findViewById(R.id.btnSearch);
        txtArriving = findViewById(R.id.txtArriving);
        txtDeparting = findViewById(R.id.txtDeparting);
        scheduleViw  = findViewById(R.id.linearLayout4);
        scheduleDropdown = findViewById(R.id.dropdown);

        btnTravel.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        scheduleDropdown.setOnItemSelectedListener(this);

        txtArriving.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, StationListActivity.class);
            startActivityForResult(intent, 1);
        });

        txtDeparting.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, StationListActivity.class);
            startActivityForResult(intent, 2);
        });

        makeDropdown();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearch:
                Intent intent1 = new Intent(MainActivity.this, SearchTrainActivity.class);
                startActivityForResult(intent1, 1);
                startActivity(intent1);
                break;

            case R.id.btnTravel:
                Intent intent2 = new Intent(MainActivity.this, TravelActivity.class);
                startActivityForResult(intent2, 2);
                startActivity(intent2);
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                Station station = (Station) data.getSerializableExtra("selectStationModel");
                String txt =  station.getStationCode() + "-" + station.getStationName();
                txtArriving.setText(txt);
            }
        }else {
            if(resultCode == RESULT_OK) {
                Station station = (Station) data.getSerializableExtra("selectStationModel");
                String txt =  station.getStationCode() + "-" + station.getStationName();
                txtDeparting.setText(txt);
            }
        }

        showScheduleView();
    }

    private void showScheduleView(){
        if (!txtArriving.getText().toString().isEmpty() && !txtDeparting.getText().toString().isEmpty()){
            scheduleViw.animate().alpha(1).setDuration(2000);
        }
    }

    private void makeDropdown(){

        ArrayList items = new ArrayList();
        items.add("5PM to 7PM");
        items.add("8PM to 10PM");
        items.add("6AM to 8AM");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, items);

        dataAdapter.setDropDownViewResource(R.layout.spinner_item);

        scheduleDropdown.setAdapter(dataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu,menu);
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position > 0){
            btnTravel.animate().alpha(1).setDuration(2000);
            btnSearch.animate().alpha(1).setDuration(2000);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
